package net.libertacasa.pubsh.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Random;

@RestController
public class API {
	
	@DeleteMapping("/backend/container/delete/{id}")
	public static String deleteContainer(@PathVariable String id, HttpServletRequest request) {
	 // [Start] This block should move to a logging method. It's only job is to print user details to the console.
	     KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
	     String username= null;
	     String userid = principal.getName();
	     IDToken token = principal.getAccount().getKeycloakSecurityContext().getIdToken();		
	     Map<String, Object> customClaims = token.getOtherClaims();
	     username = String.valueOf(customClaims.get("username"));
 	 // [End]	     
	        
		System.out.printf("Deletion triggered for ID %s by %s (%s)", id, userid, username);
		
		Docker.deleteContainer(id);
		
		return("OK, attempted deletion for " + id);
		//return("redirect:/portal");
	}
	
}