package net.libertacasa.pubsh.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.BuildImageResultCallback;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.command.ListImagesCmd;
import com.github.dockerjava.api.command.WaitContainerResultCallback;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;

public class Docker {
	private static Logger log = LoggerFactory.getLogger(Docker.class);
	
	static DockerClientConfig DockerConfig = DefaultDockerClientConfig.createDefaultConfigBuilder()
		    .withDockerHost(DockerProperties.getEndpoint())
		    .withDockerTlsVerify(false)
		    .build();	
	
	static DockerHttpClient httpClient = new ApacheDockerHttpClient.Builder()
		    .dockerHost(DockerConfig.getDockerHost())
		    .sslConfig(DockerConfig.getSSLConfig())
		    .maxConnections(100)
		    .connectionTimeout(Duration.ofSeconds(30))
		    .responseTimeout(Duration.ofSeconds(45))
		    .build();
	
	static DockerClient dockerClient = DockerClientImpl.getInstance(DockerConfig, httpClient);
	
	public static List<String> getImageChecksums() {
		log.debug("Invoked query: Image Checksums ...");
		List<Image> imageQuery;
		List<String> imageList;
		log.debug("Querying Docker API for images ...");
		imageQuery = dockerClient.listImagesCmd().exec();
		log.trace("Constructing list of images ...");
		imageList = imageQuery.stream().map(Image::getId).collect(Collectors.toList());
		log.trace("Done, returning imageList ...");
		return imageList;
	}
	
	public static List<Image> getImages(String name) {
		log.debug("Invoked query: Images ...");
		List<Image> imageQuery;
		log.trace("Assembling Docker image query ...");
		ListImagesCmd imageQueryCmd = dockerClient.listImagesCmd();
		
		log.trace("Parsing name argument ...");
		if (name != null) {
			log.debug("Found name {}, assembling query filter ...", name);
			imageQueryCmd.getFilters().put("reference", Arrays.asList(name + "*")); //to-do: filter by nameshX instead
		}
		
		log.debug("Querying Docker API for images ...");
		imageQuery = imageQueryCmd.exec();
		
		log.trace("Done, returning imageQuery ...");
		return imageQuery;
	}
	
	public static List<com.github.dockerjava.api.model.Container> getContainers(String name) {
		log.debug("Invoked query: Containers ...");
		List<com.github.dockerjava.api.model.Container> containerQuery;
		log.trace("Assembling Docker container query ...");
		ListContainersCmd containerQueryCmd = dockerClient.listContainersCmd();
		
		log.trace("Parsing name argument ...");
		if (name != null) {
			log.debug("Found name {}, assembling query filter ...", name);
			containerQueryCmd.getFilters().put("name", Arrays.asList(name + "*"));
		}
		
		log.debug("Querying Docker API for containers ...");
		containerQuery = containerQueryCmd.withShowAll(true).exec();
		
		log.trace("Done, returning containerQuery ...");
		return containerQuery;
	}
	
	public static void deleteContainer(String id) {
		log.debug("Invoked operation: Delete Container ...");
		log.trace("Converting ID to String ...");
		Collection<String> stringifiedId = Arrays.asList(id);
		//System.out.println(id);
		log.debug("Parsed ID argument: {}",stringifiedId);
		
		log.debug("Querying Docker API for running containers ...");
		List<com.github.dockerjava.api.model.Container> containerQuery = dockerClient.listContainersCmd().withShowAll(false).withIdFilter(stringifiedId).exec();
		
		log.trace("Checking container status ...");
		//apparently listContainersCmd returns a bunch of garbled crap, hence the less ideal toString/contains
		if(! containerQuery.toString().contains(id)) {
			log.debug("Not found in running containers ...");
			log.trace("Querying Docker API for all containers ...");
			List<com.github.dockerjava.api.model.Container> containerQueryAll = dockerClient.listContainersCmd().withShowAll(true).withIdFilter(stringifiedId).exec();
			if(containerQueryAll.toString().contains(id)) {
				log.debug("Found in stopped containers ...");
			}
			if(! containerQueryAll.toString().contains(id)) {
				log.warn("Container not found.");
			}
		}
		
		log.trace("Checking if found container is running ...");
		if(containerQuery.toString().contains(id)) {
			log.info("Found in running containers, stopping ...");
			log.debug("Stopping container ID {} ...",id);
			dockerClient.stopContainerCmd(id).exec();
		}

		log.debug("Removing container ID {}",id);
		dockerClient.removeContainerCmd(id).exec();
	}
	
	public static String buildImage(String targetUser, String osChoice, Integer count) {
		log.debug("Invoked operation: Build Image ...");
		String dockerfile = "/home/georg/tmp/docker/Dockerfile_" + osChoice; //to-do: move this to application.properties
		log.debug("Constructed Dockerfile location: {}", dockerfile);
		String tag = targetUser + ":" + osChoice + count;
		log.debug("Constructed image tag: {}", tag);
		log.trace("Checking if Dockerfile exists ...");
		if (! new File(dockerfile).exists()) {
			log.error("Invalid Dockerfile: {}", dockerfile);
		}
		log.trace("Constructing tags ...");
		Set<String> tags = new HashSet<String>();
		tags.add(tag);
		log.debug("Added tag {} to tags {}", tag, tags);
		log.trace("Sending image build to Docker API ...");
		String imgid = dockerClient.buildImageCmd()
				.withDockerfile(new File(dockerfile))
				.withPull(false).withNoCache(false).withTags(tags)
				.exec(new BuildImageResultCallback()).awaitImageId();
		
		log.info("Built image with ID {}", imgid);
		
		log.trace("Done, returning imgid ...");
		return(imgid);
	}
	
	public static void deleteImage(String id) {
		log.debug("Invoked operation: Delete Image ...");
		
		log.trace("Querying Docker images ...");
		List<Image> imagequery = Docker.getImages(null);

		log.trace("Checking if image exists in query ...");
		if (imagequery.toString().contains(id)) {
			try {
				log.info("Found image ID {}, deleting ...", id);
				log.trace("Sending image deletion to Docker API ...");
				dockerClient.removeImageCmd(id).withImageId(id).exec();
			} catch (com.github.dockerjava.api.exception.ConflictException exception) {
				log.warn("Image is still being used by a container.");
				log.debug("Caught exception: {}", exception.getMessage().replace("\n", ""));
				throw exception; //allow further processing by request controller
			} catch (com.github.dockerjava.api.exception.InternalServerErrorException exception) {
				if (exception.getMessage().contains("unrecognized image ID")) { //it is unclear why it sometimes returns a generic 500 with this message instead of the 404
					log.warn("Image ID not recognized.");
					log.debug("Caught exception: {}", exception.getMessage().replace("\n", ""));
					log.trace("STACK TRACE: {}", exception);
				} else if (exception.getHttpStatus() == 404) { //consider .getMessage().contains("No such image")
					log.warn("Image not found.");
					log.debug("Caught exception: {}", exception.getMessage().replace("\n", ""));
					log.trace("STACK TRACE: {}", exception);
				} else {
			    	 log.error("Failed to delete image.");
			    	 log.debug("Caught exception: {}", exception.getMessage());
			    	 log.trace("STACK TRACE: {}", exception);
				}
			} catch (Exception exception) {
		    	 log.error("Failed to delete image.");
		    	 log.debug("Caught exception: {}", exception.getMessage());
		    	 log.trace("STACK TRACE: {}", exception);
		     }
			//needs to wait for callback, but there is none
			//System.out.printf("%s", removeQuery);

		}
		
		log.trace("Checking if image does not exist in query ...");
		if (! imagequery.toString().contains(id)) {
			log.warn("Image ID {} not found.", id);
		}
		
	}
	
	public static String createContainer(String name, String imgid) {
		log.debug("Invoked operation: Create Container ...");
		//String containerid = dockerClient.createContainerCmd(imgid).exec();
		log.trace("Constructing callback ...");
		WaitContainerResultCallback resultCallback = new WaitContainerResultCallback();
		log.trace("Sending container creation to Docker API ...");
		CreateContainerResponse createContainerResponse = dockerClient
				.createContainerCmd(imgid)
				.withName(name)
				.withTty(true)
				.withAttachStdin(true)
				.exec();
		log.trace("Waiting for container creation to complete ...");
		dockerClient.waitContainerCmd(createContainerResponse.getId()).exec(resultCallback);
		log.trace("Attempting to wait for completion callback ...");
		try {
			resultCallback.awaitCompletion();
		} catch (InterruptedException exception) {
			log.error("Failed complection callback.");
			log.debug("Caught exception: {}", exception.getMessage());
		} catch (Exception exception) {
			 log.error("Failed complection callback.");
	    	 log.debug("Caught exception: {}", exception.getMessage());
	    	 log.trace("STACK TRACE: {}", exception);
	     }
		
		log.trace("Fetching container ID from creation response ...");
		String containerid = createContainerResponse.getId();
		log.debug("Created container with ID: {}", containerid);
		//System.out.println(dockerClient.infoCmd().exec());
			
		log.info("Created container with ID {} from image ID {}", containerid, imgid);
		
		log.trace("Done, returning containerid ...");
		return(containerid);
	}
	
	public static String createShell(String user, String os, Integer count) {
		log.debug("Invoked operation: Create Shell ...");
		log.trace("Constructing name ...");
		String name1 = user + "_" + os;
		String name = name1 + "_" + count;
		String imageid = null;
		log.debug("Constructed name: {}", name);
		
		log.trace("Querying Docker images ...");
		List<Image> imageQuery = Docker.getImages(user + ":" + os);

		log.trace("Image query returned: {}", imageQuery);
		
		log.trace("Checking if image query is empty ...");
		if (imageQuery.isEmpty()) {
			log.debug("Image query returned empty, building new image.");
			log.trace("Building Docker image with user -> {}, os -> {}, count -> {} ...", user, os, count);
			imageid = Docker.buildImage(user, os, count);
		}
		
		log.trace("Checking if image query is not empty ...");
		if (! imageQuery.isEmpty()) {
			log.debug("Found existing image.");
			Image image = imageQuery.get(0);
			imageid = image.getId();
			log.trace("Existing image ID {}", imageid);
		}				

		log.debug("Creating container with name {} off image with ID {}", name, imageid);
		String containerid = createContainer(name, imageid);
		log.info("Initialized shell with container ID {}", containerid);
		
		log.trace("Done, returning containerid ...");
		return(containerid);
	}
	
	public static Integer deleteShell(String user, String id) {
		log.debug("Invoked operation: Delete Shell ...");
		log.trace("Deleting container from user {} with ID {} ...", user, id);
		Docker.deleteContainer(id);
		log.trace("Querying Docker images ...");
		List<Image> images = Docker.getImages(user);
		log.trace("Parsing images ...");
		for (Image image : images) {
			log.debug("Parsing image {}", image);
			String imageid = image.getId();
			log.trace("Image ID: {}", imageid);
			try {
				try {
					log.trace("Delaying execution shortly ...");
					Thread.sleep(200);
				} catch (InterruptedException exception) {
					log.error("Failed complection callback.");
					log.debug("Caught exception: {}", exception.getMessage());
				} catch (Exception exception) {
					 log.error("Failed complection callback.");
			    	 log.debug("Caught exception: {}", exception.getMessage());
			    	 log.trace("STACK TRACE: {}", exception);
			     }
				log.debug("Deleting image with ID {} ...", imageid);
				Docker.deleteImage(imageid);
				log.trace("Deleted image.");
			} catch (com.github.dockerjava.api.exception.ConflictException exception) {
				log.info("Image is still being used by a container.");
				log.debug("Caught exception: {}", exception.getMessage().replace("\n", ""));
				throw exception;
			} catch (Exception exception) {
		    	 log.warn("Failed to delete image.");
		    	 log.debug("Caught exception: {}", exception.getMessage());
		    	 log.trace("STACK TRACE: {}", exception);
		     }
		}
		return(null);
	}

}
