package net.libertacasa.pubsh.web;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DatabaseBean {
	@Bean
	@Primary
	@ConfigurationProperties(prefix="spring.datasource")
	public static DataSource dataSource() {
	    return DataSourceBuilder.create().build();
	}
}
