package net.libertacasa.pubsh.web;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.c4_soft.springaddons.security.oauth2.test.annotations.Claims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.StringClaim;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest
@AutoConfigureMockMvc
class WebApplicationTest {

	@Test
	void contextLoads() {
	}
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void getRoot() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/"))
				.andExpect(status().isFound())
				.andExpect(redirectedUrl("/portal"));
	}	
	
    @Test
    public void getPortalNoAuth() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/portal"))
        		.andExpect(status().isFound())
        		.andExpect(redirectedUrl("/sso/login"));
    }
    
    @Test
    public void getAdminNoAuth() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/admin"))
        		.andExpect(status().isFound())
        		.andExpect(redirectedUrl("/sso/login"));
    }
    
    @Test
    @WithMockKeycloakAuth("TotallyLegitUserWithZeroAdministrativePermissions")
    public void getAdminWrongAuth() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/admin"))
        		.andExpect(status().isForbidden());
    }
    
    @Test
    @WithMockKeycloakAuth(
            authorities = { "devel-user" },
            claims = @OpenIdClaims(
            		sub = "12345",
                    email = "regular-user@example.com",
                    emailVerified = true,
                    //nickName = "TotallyLegitUserWithSuperPowers",
                    //preferredUsername = "TotallyLegitUserWithSuperPowers",
                    otherClaims = @Claims(stringClaims = @StringClaim(name = "username", value = "regular-user"))))
    public void getPortalWithAuth() throws Exception {
    	mvc.perform(MockMvcRequestBuilders.get("/portal"))
                .andExpect(status().isOk())
                .andExpect(content().string(startsWith("<!DOCTYPE html>")))
                .andExpect(content().string(containsString("Hello, <span>regular-user</span>.")))
                .andExpect(content().string(containsString("Generate new throw-away shell:")));
    }

}
